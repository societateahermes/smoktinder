import { Component } from '@angular/core';
import { Http, Response, Headers } from "@angular/http"
import { NavController } from 'ionic-angular';
import { Push, PushToken } from '@ionic/cloud-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  message = "";
  data:any;

  constructor(public http: Http, public navCtrl: NavController, public push: Push) {
    this.push.rx.notification()
      .subscribe((msg) => {
      });
  }

  notify() {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI1YzA4ZWUwYi0yMDRjLTRlNzAtOWJjZS0yNzU4YTVjZmIyM2EifQ.VhbkqjDz3wCeTNgADRCu_gSGlEkUjOynYDf4CIM-xvI";
    headers.append('Authorization', 'Bearer '+token);
    this.http.post('https://api.ionic.io/push/notifications', {
        "send_to_all": true,
        "profile": "dev",
        "notification": {
          "message": this.message
        }},{headers: headers })
      .map((response: Response) => response.json())
      .subscribe(
        (data) => this.data = data,
        (err) => this.handleError(err)
      );
  }

  handleError(error: Response){
    alert("Error! No smoking for today! :(");
  }

}
