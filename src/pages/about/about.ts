import { Component } from '@angular/core';
import {Auth} from '@ionic/cloud-angular';
import {LoginPage} from '../login/login';
import { NavController, App } from 'ionic-angular';
import { Push, PushToken } from '@ionic/cloud-angular'; 

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  constructor(private app: App, public auth:Auth, public push:Push) {

  }

  logOut() {
    this.push.unregister();
    this.auth.logout();
    this.app.getRootNav().popToRoot();
    this.app.getRootNav().push(LoginPage);
  }

}
