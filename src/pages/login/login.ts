import { Component } from '@angular/core';
import { Auth, User } from '@ionic/cloud-angular';
import { NavController, AlertController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { Push, PushToken } from '@ionic/cloud-angular';

@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})
export class LoginPage {

    constructor(public navCtrl: NavController, public auth: Auth, public alertCtrl: AlertController, 
    public push: Push, public user: User) {
        if (this.auth.isAuthenticated()) {
            this.navCtrl.push(TabsPage, {}, { animate: false })
        }
    }

    loginWithFacebook() {
        this.auth.login('facebook', { 'remember': true }).then(() => {
            this.registerForPush();
            this.navCtrl.setRoot(TabsPage);
        }, (err) => {
            this.showAlert('Error!', 'Error while authenticating using Facebook. Please try again.');
        });
    }

    loginWithTwitter() {
        this.auth.login('twitter', { 'remember': true }).then(() => {
            this.registerForPush();
            this.navCtrl.setRoot(TabsPage);
        }, (err) => {
            console.log(err);
            this.showAlert('Error!', 'Error while authenticating using Twitter. Please try again.');
        });
    }

    showAlert(title, message) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }

    registerForPush() {
        this.push.register().then((t: PushToken) => {
            return this.push.saveToken(t);
        }).then((t: PushToken) => {
        });
    }

    

}