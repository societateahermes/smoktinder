# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is a small repo for a ionic mobile application we made during hermesHackathon v2.0;
* The project started from the idea of having an app in order to gather people easily when you want to go to a smoke;
* More about ionic: http://ionic.io/developers .

### App features ###
* Login using Facebook or Twitter account;
* Send a notification with a custom message to all the users connected in the app at that moment.

### Contribution guidelines ###

* Suggest new features;
* Code review.
* If you want to contribute in any other way, talk to one of the repo admins, or send a mail to hackathon@societatea-hermes.ro with the subject "SmokTinder contribution".

### Who do I talk to? ###

* Any questions about the app, set-up process or other stuff like that can be addressed directly to one of the repo admins, or you can send a mail to hackathon@societatea-hermes.ro with the subject "SmokTinder questions/suggestions".